#!/usr/bin/env python3
# coding: utf-8

from lib import *


@VarTarget('global@kitty_str')
def kitty_target():
    value = "Kitty!"
    log("My name is " + value)
    return value


provideTarget(AggregateTarget(
    'all',
    getSubTarget("src/sub_build.py"),
    kitty_target
))
