# coding: utf-8

__all__ = ('COLOR_BLACK', 'COLOR_RED', 'COLOR_GREEN', 'COLOR_BROWN',
           'COLOR_BLUE', 'COLOR_MAGENTA', 'COLOR_CYAN', 'COLOR_LIGHTGRAY',
           'COLOR_GRAY', 'COLOR_LIGHTRED', 'COLOR_LIGHTGREEN', 'COLOR_YELLOW',
           'COLOR_LIGHTBLUE', 'COLOR_LIGHTMAGENTA', 'COLOR_LIGHTCYAN', 'COLOR_WHITE',
           'log', 'logException', 'logStatus', 'logCaption')

import sys as _sys
import traceback as _traceback


class _EscapeSequence:
    enabled = True
    
    def __init__(self, esc_):
        self.__esc = esc_
    
    @property
    def esc(self):
        return self.__esc if _EscapeSequence.enabled else ""


# noinspection PyUnresolvedReferences
if _sys.platform.startswith('win32') and _sys.getwindowsversion().major < 10:
    _EscapeSequence.enabled = False


__CLEAR_FORMAT = _EscapeSequence("\x1b[0m")

COLOR_BLACK = _EscapeSequence("\x1b[0;38;5;0m")
COLOR_RED = _EscapeSequence("\x1b[0;38;5;1m")
COLOR_GREEN = _EscapeSequence("\x1b[0;38;5;2m")
COLOR_BROWN = _EscapeSequence("\x1b[0;38;5;3m")
COLOR_BLUE = _EscapeSequence("\x1b[0;38;5;4m")
COLOR_MAGENTA = _EscapeSequence("\x1b[0;38;5;5m")
COLOR_CYAN = _EscapeSequence("\x1b[0;38;5;6m")
COLOR_LIGHTGRAY = _EscapeSequence("\x1b[0;38;5;7m")

COLOR_GRAY = _EscapeSequence("\x1b[1;38;5;8m")
COLOR_LIGHTRED = _EscapeSequence("\x1b[1;38;5;9m")
COLOR_LIGHTGREEN = _EscapeSequence("\x1b[1;38;5;10m")
COLOR_YELLOW = _EscapeSequence("\x1b[1;38;5;11m")
COLOR_LIGHTBLUE = _EscapeSequence("\x1b[1;38;5;12m")
COLOR_LIGHTMAGENTA = _EscapeSequence("\x1b[1;38;5;13m")
COLOR_LIGHTCYAN = _EscapeSequence("\x1b[1;38;5;14m")
COLOR_WHITE = _EscapeSequence("\x1b[1;38;5;15m")

# --- *** ---


def log(message_, color_=None):
    """
    Output message to stdout optionally with specified color
    (see `COLOR_*` constants).
    
    :type message_: str
    :type color_: _EscapeSequence | None
    """
    if color_:
        _sys.stdout.write("{}{}{}\n".format(color_.esc, message_, __CLEAR_FORMAT.esc))
    else:
        _sys.stdout.write("{}{}\n".format(__CLEAR_FORMAT.esc, message_))
    
    _sys.stdout.flush()


def logException(exc_info_=None):
    """
    Output specified exception information with traceback.
    Use current exception if it's not specified.
    
    :type exc_info_: tuple | None
    """
    _sys.stdout.write(__CLEAR_FORMAT.esc + "\n")
    _traceback.print_exception(*(exc_info_ or _sys.exc_info()), file=_sys.stdout)
    _sys.stdout.write("\n")
    _sys.stdout.flush()


def logStatus(status_, color_=None, percent_=None):
    """
    Output status message with predefined format and optionally with
    specified color and/or percentage.
    
    :type status_: str
    :type color_: _EscapeSequence | None
    :type percent_: float | None
    """
    if color_:
        status_ = "{}{}{}".format(color_.esc, status_, __CLEAR_FORMAT.esc)
    
    if percent_ is None:
        _sys.stdout.write("{}--- {}\n".format(__CLEAR_FORMAT.esc, status_))
    else:
        _sys.stdout.write("{}[{:4.0%}] {}\n".format(__CLEAR_FORMAT.esc, percent_, status_))
    
    _sys.stdout.flush()


def logCaption(caption_):
    """
    Output a caption of following work with predefined format and color.
    
    :type caption_: str
    """
    _sys.stdout.write("{}[-- {}{}{} --]\n".format(
        __CLEAR_FORMAT.esc, COLOR_LIGHTBLUE.esc, caption_.strip(), __CLEAR_FORMAT.esc
    ))
    _sys.stdout.flush()
