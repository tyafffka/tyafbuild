# coding: utf-8

__all__ = ('Command', 'Target')

import os as _os
import os.path as _path
import sys as _sys
import time as _time
import json as _json
import itertools as _itertools
import types as _types
import subprocess as _subprocess
import typing as _typing
import gc as _gc

from .logging import log, logException, logStatus, \
    COLOR_LIGHTMAGENTA, COLOR_LIGHTGRAY, COLOR_GREEN, COLOR_LIGHTRED
from .structure import _BaseDependency, _BaseTarget, _getModuleName, buildVars


class Command:
    """
    Class represents a subcommand (process) with automatic non-blocking wait
    to fit in maximum jobs count and for complete. Designed for use in target
    build procedure. It'll terminate process automatically at end of build.
    
    Work with it in following way: create object, wait for complete
    (for example: `while cmd.inProgress(): yield`), use the result methods
    (`returncode`, `output`). If command is not completed,
    result methods will return `None`.
    """
    __ABORT_TIMEOUT = 0.01 # s
    
    @staticmethod
    def _clear_proc(proc):
        if proc.stdin is not None:
            proc.stdin.close()
            proc.stdin = None
        
        if proc.stdout is not None:
            proc.stdout.close()
            proc.stdout = None
        
        proc.terminate()
        proc.wait(Command.__ABORT_TIMEOUT)
        proc.kill()
    
    class __Pool(list):
        def __enter__(self): return self
        
        def __exit__(self, exc_type, exc_val, exc_tb):
            for proc in self: Command._clear_proc(proc)
            self.clear()
            _gc.collect()
    
    pool = __Pool()
    
    def __init__(self, args_, in_shell_=False, catch_output_=False,
                 input_=None, working_dir_=None, environ_=None):
        """
        :type args_: list | str
        :type in_shell_: bool
        :type catch_output_: bool
        :type input_: str | bytes | None
        :type working_dir_: str | None
        :type environ_: dict | None
        """
        self.args = args_
        self.__popen_args = {
            'stdout': _subprocess.PIPE if catch_output_ else None,
            'stdin': _subprocess.PIPE if input_ is not None else None,
            'close_fds': True,
            'shell': in_shell_,
            'cwd': (working_dir_ or buildVars['buildDir']),
            'env': environ_,
            'universal_newlines': not isinstance(input_, bytes),
            'encoding': None if isinstance(input_, bytes) else _sys.stdin.encoding
        }
        self.__input = input_
        self.__process = None
    
    def __del__(self):
        if self.__process is not None:
            Command._clear_proc(self.__process)
            self.__process = None
    
    def inProgress(self):
        """
        Progress execution of the command.
        The command will not starts without calling of this method.
        Return `False` when execution of the command completed.
        
        :rtype: bool
        """
        if self.__process is None:
            if len(Command.pool) >= buildVars['maxJobs']:
                return True
            
            self.__process = _subprocess.Popen(self.args, **self.__popen_args)
            if self.__input is not None:
                self.__process.stdin.write(self.__input)
                self.__process.stdin.close()
                self.__process.stdin = None
            
            Command.pool.append(self.__process)
        
        if self.__process.poll() is not None:
            Command.pool.remove(self.__process)
            return False
        
        return True
    
    def returncode(self):
        """ :rtype: int | None """
        return self.__process or self.__process.poll()
    
    def output(self):
        """ :rtype: str | bytes | None """
        if self.__process is None: return None
        if self.__process.stdout is None: return None
        if self.__process.poll() is None: return None
        
        output = self.__process.stdout.read()
        self.__process.stdout.close()
        self.__process.stdout = None
        return output

# --- *** ---


class Target(_BaseTarget):
    """
    Base class for build target which evaluates of necessity with it
    dependencies. Direct actions must contains in overriden method `work`.
    Also pay attention for base methods `isExists`, `fingerprint` and
    `invalidate`. The class (but not subclasses) use instance cache.
    """
    __COOLDOWN_INTERVAL = 0.001 # s
    
    __STATUS_INITIAL = 0
    __STATUS_IN_PROGRESS = 1
    __STATUS_SUCCEED = 2
    __STATUS_FAILED = 3
    __STATUS_CANCELED = 4
    
    __total_targets = 0
    __built_targets = 0
    
    @staticmethod
    def __built_percent(progress_=True):
        if progress_: Target.__built_targets += 1
        return Target.__built_targets / float(Target.__total_targets)
    
    def __init__(self, unique_key_=None):
        """ :type unique_key_: str | None """
        Target.__total_targets += 1
        
        if unique_key_: _BaseTarget.__init__(self, unique_key_)
        
        if self.uniqueKey.startswith('file@'):
            filename = 'file@' + _getModuleName(
                self.uniqueKey[5:], buildVars['currentBuildDir']
            )
        else:
            filename = self.uniqueKey
        
        self.__status = self.__STATUS_INITIAL
        self.__deps_file = _path.join(
            buildVars['currentBuildDir'], 'deps@' + filename + '.json'
        )
        self.__internal_dir = _path.join(
            buildVars['currentBuildDir'], 'dir@' + filename
        )
        
        self.dependencies = [] # type: _typing.List[_BaseDependency]
        
        if self.uniqueKey in buildVars['invalidatedTargets']: self.invalidate()
    
    def __del__(self):
        if Target is not None: Target.__total_targets -= 1
    
    def getInternalDir(self):
        """
        Creates personal target directory for internal use and return it path.
        
        :rtype: str
        """
        _os.makedirs(self.__internal_dir, exist_ok=True)
        return self.__internal_dir
    
    def work(self):
        """
        This method may be overriden for doing direct job.
        Use `yield` (without a value) for suspend during long waiting
        (e.g. commands completion).
        """
        pass
    
    def isCanceled(self):
        """ :rtype: bool """
        return self.__status == self.__STATUS_CANCELED
    
    def isBuilt(self):
        """ :rtype: bool """
        return self.__status == self.__STATUS_SUCCEED
    
    def getBuilt(self):
        """
        Do all build work if needed and return it succeed.
        Distribute build of all dependencies in parallel tasks.
        
        :rtype: bool
        """
        if self.__status != self.__STATUS_INITIAL:
            return self.__status == self.__STATUS_SUCCEED
        
        _gc.collect()
        Target.__built_targets = 0
        
        with Command.pool:
            tasks = [self.__build_task()] # type: _typing.List[_typing.Optional[_typing.Generator]]
            next(tasks[0]) # get to working state
            
            while tasks:
                is_filter = False
                is_cooldown = True
                
                for i, task in enumerate(_itertools.islice(tasks, len(tasks))):
                    try:
                        sub_target = next(task)
                    except StopIteration:
                        tasks[i] = None
                        is_filter = True
                        continue
                    
                    if isinstance(sub_target, Target):
                        tasks.append(sub_target.__build_task())
                        next(tasks[-1]) # get to working state
                        is_cooldown = False
                
                if is_filter: tasks = [t for t in tasks if t is not None]
                if is_cooldown: _time.sleep(self.__COOLDOWN_INTERVAL)
        
        return self.__status == self.__STATUS_SUCCEED
    
    def __build_task(self):
        self.__status = self.__STATUS_IN_PROGRESS
        it = self.__do_build()
        
        try:
            yield # initial suspend in working state
            while True: yield next(it)
        
        except StopIteration as stop:
            self.__status = stop.value
        
        except (GeneratorExit, KeyboardInterrupt, SystemExit):
            if self.__status == self.__STATUS_IN_PROGRESS:
                logStatus("Target '{}' canceled".format(self.uniqueKey), COLOR_LIGHTGRAY, Target.__built_percent())
                self.__status = self.__STATUS_CANCELED
            raise
        
        finally:
            if self.__status == self.__STATUS_IN_PROGRESS:
                logStatus("Target '{}' failed!".format(self.uniqueKey), COLOR_LIGHTRED, Target.__built_percent())
                self.__status = self.__STATUS_FAILED
    
    def __do_build(self):
        # Evaluate dependencies:
        target_deps = [] # type: _typing.List[_typing.Optional[Target]]
        
        for dep in self.dependencies:
            if isinstance(dep, Target):
                target_deps.append(dep)
            
            elif not dep.isExists():
                logStatus("Dependency '{}' for target '{}' is not exists".format(dep.uniqueKey, self.uniqueKey), COLOR_LIGHTRED, Target.__built_percent())
                return self.__STATUS_FAILED
        
        while target_deps:
            is_filter = False
            
            for i, dep in enumerate(target_deps):
                if dep.__status == self.__STATUS_INITIAL:
                    yield dep
                
                elif dep.__status == self.__STATUS_SUCCEED:
                    target_deps[i] = None
                    is_filter = True
                    continue
                
                elif dep.__status != self.__STATUS_IN_PROGRESS:
                    logStatus("Target '{}' failed due to subtarget '{}' is not built".format(self.uniqueKey, dep.uniqueKey), COLOR_LIGHTGRAY, Target.__built_percent())
                    return self.__STATUS_FAILED
            
            if is_filter: target_deps = [d for d in target_deps if d is not None]
            yield
        
        # Check dependencies:
        is_up_to_date = self.isExists()
        
        if is_up_to_date:
            try:
                with open(self.__deps_file, 'rt') as f:
                    last_deps = _json.load(f)
                
                if not isinstance(last_deps, dict): last_deps = {}
            except IOError:
                last_deps = {}
            
            for dep in self.dependencies:
                try:
                    last_fp = last_deps.pop(dep.uniqueKey)
                except KeyError:
                    is_up_to_date = False
                    break
                
                fp = dep.fingerprint()
                if type(fp) != type(last_fp) or fp != last_fp:
                    is_up_to_date = False
                    break
            
            if last_deps: is_up_to_date = False
        
        # Build:
        if is_up_to_date:
            logStatus("Target '{}' is up-to-date".format(self.uniqueKey), COLOR_LIGHTGRAY, Target.__built_percent())
            return self.__STATUS_SUCCEED
        
        if getattr(self.work, '__func__', None) is not Target.work:
            # noinspection PyBroadException
            try:
                logStatus("Target '{}' started to build".format(self.uniqueKey), COLOR_LIGHTMAGENTA, Target.__built_percent(False))
                result = self.work()
                
                if isinstance(result, _types.GeneratorType):
                    try:
                        while True: yield next(result)
                    except StopIteration: pass
            
            except (GeneratorExit, KeyboardInterrupt, SystemExit): raise
            
            except:
                logException()
                logStatus("Target '{}' failed!".format(self.uniqueKey), COLOR_LIGHTRED, Target.__built_percent())
                return self.__STATUS_FAILED
        
        # Save dependencies:
        if self.dependencies:
            try:
                with open(self.__deps_file, 'wt') as f:
                    _json.dump({dep.uniqueKey: dep.fingerprint()
                                for dep in self.dependencies}, f, indent=2)
                    f.write("\n")
            
            except IOError:
                log("Warning: target '{}' dependencies fingerprints is not saved!".format(self.uniqueKey))
        else:
            try: _os.unlink(self.__deps_file)
            except OSError: pass
        
        logStatus("Target '{}' built".format(self.uniqueKey), COLOR_GREEN, Target.__built_percent())
        return self.__STATUS_SUCCEED
