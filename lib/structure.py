# coding: utf-8

__all__ = ('buildVars', 'provideTarget', 'getSubTarget')

import os as _os
import os.path as _path
import sys as _sys
import json as _json
import collections as _collections
import typing as _typing
import importlib as _importlib
import importlib.util

from .logging import log, logCaption, logStatus, logException, \
    COLOR_LIGHTGREEN, COLOR_LIGHTRED


class _BaseDependency(object):
    __instances_cache = {}
    
    def __new__(cls, unique_key_):
        try:
            return cls.__instances_cache[unique_key_]
        except KeyError:
            return object.__new__(cls)
    
    def __init__(self, unique_key_):
        self.uniqueKey = unique_key_ # type: str
        _BaseDependency.__instances_cache[unique_key_] = self
    
    def isExists(self): return False
    def fingerprint(self): return None


class _BaseTarget(_BaseDependency):
    def isCanceled(self): return False
    def isBuilt(self): return False
    def getBuilt(self): return True
    def invalidate(self): pass


class _TBuildVars(_collections.MutableMapping):
    _MASKED_KEYS = {
        'buildVariant', 'maxJobs', 'invalidatedTargets', 'currentModule',
        'sourceRoot', 'currentSourceDir', 'buildRoot', 'currentBuildDir'
    }
    
    _is_protected = False
    
    class _Globals(dict):
        def __setitem__(self, key_, value_):
            if _TBuildVars._is_protected:
                if '@' in key_:
                    raise NameError("Global build variable can't be named with a prefix")
                elif key_ in _TBuildVars._MASKED_KEYS:
                    raise NameError("Assign of key '{}' is not permitted".format(key_))
                elif not isinstance(value_, (type(None), bool, int, str, list, tuple, dict)):
                    raise ValueError("Global build variable value must be JSON-serializable")
            
            dict.__setitem__(self, key_, value_)
    
    def __init__(self):
        self.__global_frame = _TBuildVars._Globals()
        self.__current_frame = {}
    
    def __getitem__(self, key_):
        if key_.startswith('global@'): return self.__global_frame[key_[7:]]
        elif key_.startswith('env@'): return _os.environ[key_[4:]]
        elif key_.startswith('var@'): return self.__current_frame[key_[4:]]
        
        try: return self.__current_frame[key_]
        except KeyError: return self.__global_frame[key_]
    
    def __setitem__(self, key_, value_):
        if key_.startswith('var@'): key_ = key_[4:]
        
        if '@' in key_:
            raise NameError("Local build variable can only has a prefix 'var@'")
        
        if _TBuildVars._is_protected and key_ in _TBuildVars._MASKED_KEYS:
            raise NameError("Assign of key '{}' is not permitted".format(key_))
        
        self.__current_frame[key_] = value_
    
    def __delitem__(self, key_):
        try: del self.__current_frame[key_]
        except KeyError: pass
    
    def __len__(self): return len(self.__current_frame)
    
    def __iter__(self): return iter(self.__current_frame)
    
    @property
    def globals(self): return self.__global_frame
    
    @property
    def locals(self): return self.__current_frame
    
    def copy(self):
        cp = object.__new__(_TBuildVars)
        cp.__global_frame = self.__global_frame
        cp.__current_frame = self.__current_frame
        return cp
    
    def _stepIn(self):
        prev_frame = self.__current_frame
        self.__current_frame = prev_frame.copy()
        return prev_frame
    
    def _stepOut(self, prev_frame_):
        self.__current_frame = prev_frame_

# ---


class __SubModuleEnd(BaseException):
    def __init__(self, target_): self.target = target_


__BUILD_VARS_FILE = "globalBuildVars.json"

__is_sub_module = False


def _getModuleName(path_, relative_to_):
    """
    :type path_: str
    :type relative_to_: str
    :rtype: str
    """
    parts = _path.relpath(path_, relative_to_).split(_path.sep) # type: _typing.List[str]
    if not parts[0] or parts[0] == '.': return ''
    
    n_back = 0
    for p in parts:
        if p != '..': break
        n_back += 1
    
    res = '.'.join(p for i, p in enumerate(parts) if i >= n_back)
    if n_back > 0: res = '.' * n_back + res
    return res


def _init(build_dir_, build_vars_):
    # Predefined build variables:
    
    script = _path.abspath(_sys.argv[0])
    buildVars['currentModule'] = _path.splitext(_path.basename(script))[0]
    
    buildVars.globals['sourceRoot'] = _path.dirname(script)
    buildVars['currentSourceDir'] = buildVars['sourceRoot']
    
    buildVars.globals['buildRoot'] = _path.abspath(build_dir_)
    buildVars['currentBuildDir'] = buildVars['buildRoot']
    
    _os.makedirs(buildVars['currentBuildDir'], exist_ok=True)
    _os.chdir(buildVars['currentBuildDir'])
    
    _TBuildVars._is_protected = True
    
    # Saved build variables:
    try:
        with open(_path.join(buildVars['buildRoot'], __BUILD_VARS_FILE), 'rt') as f:
            saved_build_vars = _json.load(f)
        
        if isinstance(saved_build_vars, dict):
            buildVars.globals.update(saved_build_vars)
    
    except IOError: pass
    
    buildVars.globals.update(build_vars_)
    
    logCaption("Configuring " + buildVars['buildVariant'])

# --- *** ---


buildVars = _TBuildVars()
"""
Dictionary of variables used as data source for an evaluates.
It has two scopes: global and current module local variables.
Global variables can be read explicitly with prefix `global@`.
Additionally it can read environment variables with prefix `env@`.
Some special variables is used by the build system for configure self work and
files structure. It's following:

- `buildVariant` (global) - value passed as first parameter for script if any;
- `maxJobs` (global) - maximum number of subcommands currently running;
- `invalidatedTargets` (global) - set of unique keys of targets which will be
  invalidated at creation;
- `currentModule` (local) - namespace-like name of current module;
- `sourceRoot` (global) - path of dir where located top module script;
- `currentSourceDir` (local) - path of dir where located current module script;
- `buildRoot` (global) - path of root dir used for intermediate files;
- `currentBuildDir` (local) - path of build dir corresponding to current module.
"""


def provideTarget(target_to_build_):
    """
    Complete cunfiguring of current module by taking provided target.
    If it's root module, complete building by evaluating this target,
    otherwise return it from `getSubTarget` for upper module. Never returns.
    
    :type target_to_build_: _BaseTarget
    """
    if not isinstance(target_to_build_, _BaseTarget):
        raise ValueError("Provided object is not a target")
    
    if __is_sub_module: raise __SubModuleEnd(target_to_build_)
    
    # noinspection PyBroadException
    try:
        logCaption("Building " + buildVars['buildVariant'])
        is_built = target_to_build_.getBuilt()
    
    except KeyboardInterrupt:
        logStatus("Build interrupted", COLOR_LIGHTRED)
        raise SystemExit(1)
    
    except SystemExit: raise
    
    except:
        logException()
        logStatus("Build failed with exception!", COLOR_LIGHTRED)
        raise SystemExit(3)
    
    try:
        with open(_path.join(buildVars['buildRoot'], __BUILD_VARS_FILE), 'wt') as f:
            _json.dump({k: v for k, v in buildVars.globals.items()
                        if k not in _TBuildVars._MASKED_KEYS}, f, indent=2)
            f.write("\n")
    
    except IOError:
        log("Warning: build variables is not saved!")
    
    if is_built:
        logStatus("Build finished", COLOR_LIGHTGREEN)
        raise SystemExit(0)
    else:
        logStatus("Build failed!", COLOR_LIGHTRED)
        raise SystemExit(3)


def getSubTarget(module_file_):
    """
    Call submodule from file `module_file_` which located relative to
    current module, and return target provided by it. Current source and
    build directories steps in appropriately to submodule location.
    Current build directory is also always the process working directory
    (but only at configuring time!).
    
    :type module_file_: str
    :rtype: _BaseTarget
    """
    global __is_sub_module
    
    module_location = _path.normpath(_path.join(
        buildVars['currentSourceDir'], module_file_
    ))
    module_noext = _path.splitext(module_location)[0]
    
    prev_is_sub_module, __is_sub_module = __is_sub_module, True
    prev_frame = buildVars._stepIn()
    _TBuildVars._is_protected = False
    
    try:
        buildVars['currentModule'] = _getModuleName(
            module_noext, buildVars['sourceRoot']
        )
        buildVars['currentSourceDir'] = _path.dirname(module_location)
        buildVars['currentBuildDir'] = _path.join(
            prev_frame['currentBuildDir'], "module@" + _getModuleName(
                module_noext, prev_frame['currentSourceDir']
            )
        )
        _TBuildVars._is_protected = True
        
        _os.makedirs(buildVars['currentBuildDir'], exist_ok=True)
        _os.chdir(buildVars['currentBuildDir'])
        
        spec = _importlib.util.spec_from_file_location(
            '__tyaf_build_module.' + buildVars['currentModule'], module_location
        )
        module = _importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
    
    except __SubModuleEnd as end:
        return end.target
    
    finally:
        __is_sub_module = prev_is_sub_module
        buildVars._stepOut(prev_frame)
        _TBuildVars._is_protected = True
        
        _os.chdir(buildVars['currentBuildDir'])
    
    raise RuntimeError("No target provided by submodule in '{}'".format(module_location))
