# coding: utf-8
"""
tyafBuild is a generic software build system with dependencies scheduling.
It may be used widely for various tasks from configuring to packaging.
It intended as a library for build scripts.

The system is object-oriented and it basis is a targets objects, which may
represents a file or a data item (known as "build variable") and may has
other targets as dependencies. Also, a build scripts can be implemented with
submodules with easy collenting of targets from they (using `getSubTarget`
function).

Once a main target is created with all it dependencies, it must be passed to
`provideTarget` function, which will build it.

Requires Python version 3.5.
"""

__author__ = 'ТяФ/ка (tyafffka)'
__version__ = 'v1.0'

from .logging import *
from .structure import *
from .targets import *
from .lib_targets import *


def __init():
    import argparse
    from .structure import _init as structure_init
    from .logging import _EscapeSequence
    
    arg_parser = argparse.ArgumentParser(
        description="Developed using tyafBuild {}".format(__version__)
    )
    arg_parser.add_argument('variant', nargs='?', default="",
        help="Build variant used by script for select appropriate target or options"
    )
    arg_parser.add_argument('-B', '--build-dir', nargs=1, default="build", metavar='DIR',
        help="Directory which will be used for store intermediate files (default is 'build' subdir)"
    )
    arg_parser.add_argument('-j', '--jobs', nargs=1, default=1, type=int, metavar='N',
        help="Specifies the maximum number of subcommands that can run simultaneously (default 1)"
    )
    arg_parser.add_argument('-D', '--define', nargs='+', default=[], metavar='var=value',
        help="Defines initial values of specified global build variables"
    )
    arg_parser.add_argument('--invalidate', nargs='+', default=[], metavar='key',
        help="Unique keys of targets which will be invalidated immediately"
    )
    arg_parser.add_argument('--no-color', default=False, action='store_true',
        help="Disable colorizing of script output"
    )
    arguments = arg_parser.parse_args()
    
    # ---
    
    vars__ = {}
    for define in arguments.define:
        kv_pair = define.split('=', 1)
        
        if len(kv_pair) < 2 or ':' in kv_pair[0]:
            arg_parser.exit(2, arg_parser.format_usage() +
                "{}: error: wrong format of argument -D/--define\n".format(arg_parser.prog)
            )
        
        vars__[kv_pair[0]] = kv_pair[1]
    
    buildVars.globals['buildVariant'] = arguments.variant
    buildVars.globals['maxJobs'] = max(arguments.jobs, 1)
    buildVars.globals['invalidatedTargets'] = set(arguments.invalidate)
    
    if arguments.no_color: _EscapeSequence.enabled = False
    
    structure_init(arguments.build_dir, vars__)


__init()
