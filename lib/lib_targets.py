# coding: utf-8

__all__ = ('VarDependency', 'FileDependency',
           'AggregateTarget', 'VarTarget', 'FileTarget',
           'GenerateFileTarget', 'JinjaTarget',
           'VarByCommandTarget', 'FileByCommandTarget')

import os as _os
import os.path as _path
import types as _types
import hashlib as _hashlib

try: import jinja2 as _jinja
except ImportError: _jinja = None

from .structure import _BaseDependency, buildVars
from .targets import Target, Command


class VarDependency(_BaseDependency):
    """
    Pure dependency object that represents a build variable. Any key that can
    be read from `buildVars`, may be used. The object remembers current scope.
    The class (but not subclasses) use instance cache.
    """
    @staticmethod
    def __dep_key(var_name_):
        """
        :type var_name_: str
        :rtype: str
        """
        if '@' not in var_name_:
            return 'var@' + var_name_ + '@' + buildVars['currentModule']
        elif var_name_.startswith('var@') and var_name_.find('@', 4) < 0:
            return var_name_ + '@' + buildVars['currentModule']
        else:
            return var_name_
    
    def __new__(cls, var_name_):
        return _BaseDependency.__new__(cls, VarDependency.__dep_key(var_name_))
    
    def __init__(self, var_name_):
        """ :type var_name_: str """
        _BaseDependency.__init__(self, VarDependency.__dep_key(var_name_))
        
        if var_name_.startswith('var@'): var_name_ = var_name_[4:]
        
        if var_name_.startswith('global@'):
            self._frame = buildVars.globals
            self._var_name = var_name_[7:]
        elif var_name_.startswith('env@'):
            self._frame = _os.environ
            self._var_name = var_name_[4:]
        elif '@' not in var_name_:
            self._frame = buildVars.locals
            self._var_name = var_name_
        else:
            raise ValueError("Invalid key of a variable")
    
    def isExists(self):
        return self._var_name in self._frame
    
    def fingerprint(self):
        try: return self._frame[self._var_name]
        except KeyError: return ""
    
    @property
    def value(self): return self._frame[self._var_name]
    
    @value.setter
    def value(self, value_): self._frame[self._var_name] = value_
    
    @value.deleter
    def value(self): del self._frame[self._var_name]


class FileDependency(_BaseDependency):
    """
    Pure dependency object that represents a file. Please,
    specify absolute path to file because it will be used at build time.
    The class (but not subclasses) use instance cache.
    """
    BY_TIMESTAMP = 0
    BY_MD5 = 1
    BY_SHA1 = 2
    
    def __new__(cls, filepath_):
        return _BaseDependency.__new__(cls, 'file@' + filepath_)
    
    def __init__(self, filepath_):
        """ :type filepath_: str """
        _BaseDependency.__init__(self, 'file@' + filepath_)
        
        self.filepath = filepath_
        self.fingerprintBy = FileDependency.BY_TIMESTAMP
    
    def isExists(self):
        return _path.exists(self.filepath)
    
    def fingerprint(self):
        try:
            if self.fingerprintBy == FileDependency.BY_TIMESTAMP:
                return _os.stat(self.filepath).st_mtime
            
            elif self.fingerprintBy == FileDependency.BY_MD5:
                return _hashlib.md5(self.readBinary()).hexdigest()
            
            elif self.fingerprintBy == FileDependency.BY_SHA1:
                return _hashlib.sha1(self.readBinary()).hexdigest()
            
            else: raise ValueError("Unknown fingerprint type")
        
        except OSError: return None
    
    def read(self):
        with open(self.filepath, 'rt') as f: return f.read()
    
    def readBinary(self):
        with open(self.filepath, 'rb') as f: return f.read()
    
    def write(self, data_):
        with open(self.filepath, 'wt' if isinstance(data_, str) else 'wb') as f:
            f.write(data_)
            if isinstance(data_, str) and data_ and data_[-1] != '\n':
                f.write('\n')

# --- *** ---


class AggregateTarget(Target):
    """
    Target object class which just aggregate other targets by self
    dependencies. It's always exists itself.
    Name of the target also must be unique with other pure targets.
    """
    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)
    
    def __init__(self, name_, *dependencies_):
        """
        :type name_: str
        :type dependencies_: _BaseDependency
        """
        Target.__init__(self, name_)
        
        if dependencies_: self.dependencies.extend(dependencies_)
    
    def isExists(self): return True
    
    def invalidate(self):
        for dep in self.dependencies:
            if isinstance(dep, Target): dep.invalidate()


class VarTarget(Target, VarDependency):
    """
    Target object class that represents calculated build variable.
    Method `calculate` must be implemented for get value using dependencies.
    Also, the object may be used as decorator on freestanding `calculate`
    function, like so:
    ```
    @VarTarget('my_variable', dep1, dep2)
    def my_variable_target():
        ...
        return "..."
    ```
    """
    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)
    
    def __init__(self, var_name_, *dependencies_):
        """
        :type var_name_: str
        :type dependencies_: _BaseDependency
        """
        VarDependency.__init__(self, var_name_)
        Target.__init__(self)
        
        if dependencies_: self.dependencies.extend(dependencies_)
    
    def __call__(self, func_):
        if getattr(self.calculate, '__func__', None) is VarTarget.calculate:
            self.calculate = func_
        return self
    
    def calculate(self):
        """
        Method for get actual value of the variable. Override it or use
        the object as decorator on function which replace it.
        """
        return ""
    
    def work(self):
        result = self.calculate()
        
        if isinstance(result, _types.GeneratorType):
            try:
                while True: yield next(result)
            except StopIteration as stop:
                result = stop.value
        
        self.value = result
    
    def invalidate(self):
        try: del self.value
        except KeyError: pass


class FileTarget(Target, FileDependency):
    """
    Target object class that represents a file which will be built. If the
    file path is relative, it considered to be from current build directory.
    Method `work` must be implemented for create target file. Also, the object
    may be used as decorator on freestanding `work` function, like so:
    ```
    @FileTarget('my_file.txt', dep1, dep2)
    def my_file_target():
        ...
        with open(my_file_target.filepath, 'w') as f:
            ...
    ```
    """
    def __new__(cls, *args, **kwargs):
        return object.__new__(cls)
    
    def __init__(self, filepath_, *dependencies_):
        """
        :type filepath_: str
        :type dependencies_: _BaseDependency
        """
        if not _path.isabs(filepath_):
            filepath_ = _path.join(buildVars['currentBuildDir'], filepath_)
        filepath_ = _path.normpath(filepath_)
        
        FileDependency.__init__(self, filepath_)
        Target.__init__(self)
        
        if dependencies_: self.dependencies.extend(dependencies_)
    
    def __call__(self, func_):
        if getattr(self.work, '__func__', None) is Target.work:
            self.work = func_
        return self
    
    def invalidate(self):
        try: _os.unlink(self.filepath)
        except OSError: pass

# --- *** ---


class GenerateFileTarget(FileTarget):
    """
    Target object class that generate textual file from template by substitute
    build variable names in format `[%<var-name>%]` by their values.
    Optionally a format attributes (like in standard format function) or
    join separator (in case of container value) may be specified after `|`
    inside brackets. If source file path is relative, it considered to be
    from current source directory.
    """
    def __init__(self, dest_file_, source_file_, *dependencies_):
        """
        :type dest_file_: str
        :type source_file_: str
        :type dependencies_: _BaseDependency
        """
        FileTarget.__init__(self, dest_file_, *dependencies_)
        
        if not _path.isabs(source_file_):
            source_file_ = _path.join(buildVars['currentSourceDir'], source_file_)
        
        self.sourceFile = _path.normpath(source_file_)
        self.vars = buildVars.copy()
        
        self.dependencies.append(FileDependency(self.sourceFile))
    
    def work(self):
        with open(self.sourceFile, 'rt') as f:
            source = f.read()
        
        with open(self.filepath, 'wt') as f:
            sub_prev = 0
            
            while True:
                sub_begin, sub_end, sub = self.find_next(source, sub_prev)
                if sub_begin is None: break
                
                f.write(source[sub_prev:sub_begin])
                if sub is not None: f.write(sub)
                sub_prev = sub_end
            
            if sub_prev < len(source):
                f.write(source[sub_prev:] if sub_prev > 0 else source)
    
    def find_next(self, str_, begin_):
        """
        :type str_: str
        :type begin_: int
        :rtype: tuple[int | None, int | None, str | None]
        """
        found_start = str_.find('[%', begin_)
        if found_start < 0: return None, None, None
        found_start += 2
        
        found_end = str_.find('%]', found_start)
        if found_end < 0: return None, None, None
        void_sub = (found_start - 2, found_end + 2, None)
        
        found_spec = str_.find('|', found_start, found_end)
        
        if found_spec > 0:
            try: var = self.vars[str_[found_start:found_spec].strip()]
            except KeyError: return void_sub
            
            if isinstance(var, (list, tuple, dict)):
                sub = str_[found_spec + 1:found_end].join(str(i) for i in var)
            else:
                sub = ("{:%s}" % str_[found_spec + 1:found_end]).format(var)
        else:
            try: var = self.vars[str_[found_start:found_end].strip()]
            except KeyError: return void_sub
            
            sub = str(var)
        
        return found_start - 2, found_end + 2, sub


if _jinja:
    class JinjaTarget(FileTarget):
        """
        Target object class that generate textual file from Jinja template.
        Need Jinja2 to be installed. Following objects is available
        inside templates: `buildVars`, `environ` and `target`.
        Use current source directory for Jinja environment.
        """
        __env_cache = {}
        
        def __init__(self, dest_file_, source_file_, *dependencies_):
            """
            :type dest_file_: str
            :type source_file_: str
            :type dependencies_: _BaseDependency
            """
            FileTarget.__init__(self, dest_file_, *dependencies_)
            
            source_dir = buildVars['currentSourceDir']
            if not _path.isabs(source_file_):
                source_file_ = _path.join(source_dir, source_file_)
            
            self.sourceFile = _path.normpath(source_file_)
            self.vars = buildVars.copy()
            
            if source_dir in JinjaTarget.__env_cache:
                self.jinjaEnvironment = JinjaTarget.__env_cache[source_dir]
            else:
                self.jinjaEnvironment = _jinja.Environment(
                    loader=_jinja.FileSystemLoader(source_dir), autoescape=False
                )
                JinjaTarget.__env_cache[source_dir] = self.jinjaEnvironment
            
            self.dependencies.append(FileDependency(source_file_))
        
        def work(self):
            with open(self.sourceFile, 'rt') as f:
                source = f.read()
            
            generator = self.jinjaEnvironment.from_string(source).generate(
                buildVars=self.vars, environ=_os.environ, target=self
            )
            
            with open(self.filepath, 'wt') as f:
                for line in generator: f.write(line)
                if source and source[-1] == "\n": f.write("\n") # hint against Jinja!

# --- *** ---


# noinspection PyUnresolvedReferences
class _MixinByCommandTarget:
    def _on_init(self, args_):
        if isinstance(args_, list):
            for arg in args_:
                if isinstance(arg, _BaseDependency):
                    self.dependencies.append(arg)
        
        elif isinstance(args_, _BaseDependency):
            self.dependencies.append(args_)
    
    def _on_work(self):
        if isinstance(self.command.args, list):
            for i, arg in enumerate(self.command.args):
                if isinstance(arg, VarDependency):
                    self.command.args[i] = arg.value
                
                elif isinstance(arg, FileDependency):
                    self.command.args[i] = arg.filepath
        
        elif isinstance(self.command.args, VarDependency):
            self.command.args = self.command.args.value
        
        elif isinstance(self.command.args, FileDependency):
            self.command.args = self.command.args.filepath


class VarByCommandTarget(VarTarget, _MixinByCommandTarget):
    """
    Target object class that assign a build variable value from
    output of a command. A dependency object may be passed as any command
    argument, in this case it will be appended to dependencies list.
    Also pay attention for `postCalc` method.
    """
    def __init__(self, var_name_, args_, in_shell_=False,
                 input_=None, working_dir_=None, environ_=None):
        """
        :type var_name_: str
        :type args_: list | str | _BaseDependency
        :type in_shell_: bool
        :type input_: str | None
        :type working_dir_: str | None
        :type environ_: dict | None
        """
        VarTarget.__init__(self, var_name_)
        _MixinByCommandTarget._on_init(self, args_)
        
        self.command = Command(args_, in_shell_, True, input_, working_dir_, environ_)
    
    def calculate(self):
        _MixinByCommandTarget._on_work(self)
        
        while self.command.inProgress(): yield
        
        rc = self.command.returncode()
        if rc != 0:
            raise ValueError("Command in target '{}' failed with return code: {}".format(self.uniqueKey, rc))
        
        return self.postCalc(self.command.output())
    
    # noinspection PyMethodMayBeStatic
    def postCalc(self, value_): return value_.strip()


class FileByCommandTarget(FileTarget, _MixinByCommandTarget):
    """
    Target object class that get a file by invoke of a command.
    A dependency object may be passed as any command argument and
    it will be appended to dependencies list.
    """
    def __init__(self, filepath_, args_, in_shell_=False,
                 input_=None, working_dir_=None, environ_=None):
        """
        :type filepath_: str
        :type args_: list | str | _BaseDependency
        :type in_shell_: bool
        :type input_: str | bytes | None
        :type working_dir_: str | None
        :type environ_: dict | None
        """
        FileTarget.__init__(self, filepath_)
        _MixinByCommandTarget._on_init(self, args_)
        
        self.command = Command(args_, in_shell_, False, input_, working_dir_, environ_)
    
    def work(self):
        _MixinByCommandTarget._on_work(self)
        
        while self.command.inProgress(): yield
        
        rc = self.command.returncode()
        if rc != 0:
            raise ValueError("Command in target '{}' failed with return code: {}".format(self.uniqueKey, rc))
